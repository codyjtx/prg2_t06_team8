﻿
//============================================================
// Student Number : S10204599
// Student Name : Ng Lynn Shaan
// Module Group : T06
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace PRG_Assessment
{
    class SHNFacility
    {
        public string FacilityName { get; set; }
        public int FacilityCapacity { get; set; }
        public int FacilityVacancy { get; set; }
        public double distFromAirCheckpoint { get; set; }
        public double distFromSeaCheckpoint { get; set; }
        public double distFromLandCheckpoint { get; set; }

        public SHNFacility()
        {

        }
        public SHNFacility(string fn, int fc, double dac, double dsc, double dlc)
        {
            FacilityName = fn;
            FacilityCapacity = fc;
            distFromAirCheckpoint = dac;
            distFromSeaCheckpoint = dsc;
            distFromLandCheckpoint = dlc;
        }
        public double CalculateTravelCost(string em, DateTime ed)
        {
            double distance = 0;
            double costs = 50;
            int time = ed.Hour;
            if (em == "Air")
            {
                distance = distFromAirCheckpoint;
                costs = costs + (distance*0.25);
            
                if (6 < time & time < 9 | 18 < time & time < 24)
                {
                    costs = costs + (costs * 0.25);
                    return costs;
                }
                if (0 < time & time < 6)
                {
                    costs = costs + (costs * 0.25);
                    return costs;
                }

                return costs;
            }

            else if (em == "Land")
            {
                distance = distFromLandCheckpoint;
                costs = costs + (distance*0.25);
            
                if (6 < time & time < 9 | 18 < time & time < 24)
                {
                    costs = costs + (costs * 0.25);
                    return costs;
                }
                if (0 < time & time < 6)
                {
                    costs = costs + (costs * 0.25);
                    return costs;
                }

                return costs;
            }

            else if (em == "Sea")
            {
                distance = distFromSeaCheckpoint;
                costs = costs + (distance*0.25);
            
                if (6 < time & time < 9 | 18 < time & time < 24)
                {
                    costs = costs + (costs * 0.25);
                    return costs;
                }
                if (0 < time & time < 6)
                {
                    costs = costs + (costs * 0.25);
                    return costs;
                }

                return costs;
            }
            else
            {
                return costs;
            }
        }
        
        public bool IsAvailable(int fac, int fav)
        {
            if (fac == fav)
            {
                return false;
            }
            else return true;
        }
        public override string ToString()
        {
            return string.Format("{0,-10}", FacilityName);
        }
    }
}
