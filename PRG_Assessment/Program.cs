﻿
//============================================================
// Student Number : S10155754, S10204599
// Student Name : Jeow Teng Xiang Cody, Ng Lynn Shaan 
// Module Group : T06
//============================================================

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace PRG_Assessment
{
    class Program
    {
        static void Main(string[] args)
        {
            // Person Lists
            List<Person> personList = new List<Person>();
            // Business List
            List<BusinessLocation> locationList = new List<BusinessLocation>();
            // Facilities List
            List<SHNFacility> shnList = new List<SHNFacility>();
            
            // Menu Options 

            while (true)
            {
                int userInput = -1;
                try
                {
                    Menu();
                    Console.Write("Select an option: ");
                    userInput = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine();
                }
                catch (FormatException)
                {
                    Console.WriteLine("You did not input a number");
                }

                if (userInput == 1)
                {
                    if (shnList.Count == 0)
                    {
                        Console.WriteLine("Please load SHN data first");
                    }
                    else if (personList.Count > 0)
                    {
                        Console.WriteLine("Data has already been loaded");
                    }
                    else
                    {
                        LoadPerson(personList,shnList);
                        LoadBusiness(locationList);
                    }
                }

                else if (userInput == 2)
                {
                    if (shnList.Count > 0)
                    {
                        Console.WriteLine("Data has already been loaded");
                    }
                    else
                    {
                        LoadShnData(shnList);
                    }
                }

                else if (userInput == 3)
                {
                    if (personList.Count == 0)
                    {
                        Console.WriteLine("Please load Person data first");
                    }
                    else
                    {
                        ListVisitors(personList);
                    }
                }

                else if (userInput == 4)
                {
                    if (personList.Count == 0)
                    {
                        Console.WriteLine("Please load Person data first");
                    }
                    else
                    {
                        Console.Write("Enter your name: ");
                        string search = Console.ReadLine();
                        ListDetails(personList, search);
                    }
                }

                else if (userInput == 5)
                {
                    if (personList.Count == 0)
                    {
                        Console.WriteLine("Please load Person data first");
                    }
                    else
                    {
                        AssignOrReplaceToken(personList);
                    }
                }

                else if (userInput == 6)
                {
                    if (locationList.Count == 0)
                    {
                        Console.WriteLine("Please load Business Location data first");
                    }
                    else
                    {
                        ListBusiness(locationList);
                    }
                }

                else if (userInput == 7)
                {
                    if (locationList.Count == 0)
                    {
                        Console.WriteLine("Please load Business Location data first");
                    }
                    else
                    {
                        ListBusiness(locationList);
                        EditCapacity(locationList);
                    }
                }

                else if (userInput == 8)
                {
                    if (locationList.Count == 0)
                    {
                        Console.WriteLine("Please load Person and Business Location data first");
                    }
                    else
                    {
                        CheckIn(personList, locationList);
                    }
                }

                else if (userInput == 9)
                {
                    if (personList.Count == 0)
                    {
                        Console.WriteLine("Please load Person data first");
                    }
                    else
                    {
                        CheckOut(personList);
                    }
                }

                else if (userInput == 10)
                {
                    if (shnList.Count == 0)
                    {
                        Console.WriteLine("Please load SHN Facility data first");
                    }
                    else
                    {
                        Console.WriteLine("{0,-20}  {1,-10}  {2,-10}  {3,-10}  {4,0}",
                            "Facil Name", "Facil Capacity", "Dist From Air", "Dist From Land", "Dist From Sea");
                        ListShn(shnList);
                    }
                }

                else if (userInput == 11)
                {
                    CreateVisitor(personList);
                }

                else if (userInput == 12)
                {
                    if (personList.Count == 0 || shnList.Count == 0)
                    {
                        Console.WriteLine("Please load both Person and SHN Facility data first");
                    }
                    else
                    {
                        CreateTravelEntry(personList, shnList);
                    }
                }

                else if (userInput == 13)
                {
                    if (personList.Count == 0)
                    {
                        Console.WriteLine("Please load Person data first");
                    }
                    else
                    {   
                        CalculateShnCharges(personList);
                    }
                }

                else if (userInput == 14)
                {
                    if (personList.Count == 0)
                    {
                        Console.WriteLine("Please load Person data first");
                    }
                    else
                    {   
                        ContactTracingReport(personList);
                    }
                }

                else if (userInput == 15)
                {
                    ShnStatusReport(personList);
                }

                else if (userInput == 16)
                {
                    CreateBusiness(locationList);
                }

                else if (userInput == 17)
                {
                    CreateFacility(shnList);
                }

                else if (userInput == 18)
                {
                    ListFacility(shnList, personList);
                }

                else if (userInput == 19)
                {
                    MoveFacility(personList, shnList);
                }

                else if (userInput == 0)
                {
                    Console.WriteLine("Thank You for using the Covid Monitoring System");
                    Console.WriteLine("Now Exiting........");
                    break;
                }

                else
                {
                    
                }
            }
            
        }


        // Menu 
        static void Menu()
        {
            Console.WriteLine();
            Console.WriteLine("------------* MENU *-------------");
            Console.WriteLine();
            Console.WriteLine("------------ GENERAL ------------");
            Console.WriteLine("[ 1]  Load Person and Business details ");
            Console.WriteLine("[ 2]  Load Shn Data");
            Console.WriteLine("[ 3]  List All Visitors");
            Console.WriteLine("[ 4]  List Person Details");
            Console.WriteLine();
            Console.WriteLine("---------- SAFE ENTRY -----------");
            Console.WriteLine("[ 5]  Assign/Replace TraceTogether Token");
            Console.WriteLine("[ 6]  List all Business Locations");
            Console.WriteLine("[ 7]  Edit Business Location Capacity");
            Console.WriteLine("[ 8]  SafeEntry Check-In");
            Console.WriteLine("[ 9]  SafeEntry Check-Out");
            Console.WriteLine();
            Console.WriteLine("--------- TRAVEL ENTRY ----------");
            Console.WriteLine("[10]  List all SHN Facilities");
            Console.WriteLine("[11]  Create Visitor");
            Console.WriteLine("[12]  Create TravelEntry Record");
            Console.WriteLine("[13]  Calculate SHN Charges");
            Console.WriteLine();
            Console.WriteLine("---------- ADDITIONAL -----------");
            Console.WriteLine("[14]  Contact Tracing Report");
            Console.WriteLine("[15]  SHN Status Report");
            Console.WriteLine("[16]  Create Business Location");
            Console.WriteLine("[17]  Create Facility");
            Console.WriteLine("[18]  List Facility Persons");
            Console.WriteLine("[19]  Move Person");
            Console.WriteLine();
            Console.WriteLine("[ 0]  Exit");
            Console.WriteLine("---------------------------------");
            Console.WriteLine();
        }

        // Search Functions: 
        
        // Search Person
        static Person SearchPerson(List<Person> pList, string na)
        {
            foreach (var p in pList)
            {
                if (na == p.Name)
                {
                    return p;
                }
            }
            return null;
        }
        
        
        // Search Businesses
        static BusinessLocation SearchBusiness(List<BusinessLocation> bList, string na)
        {
            foreach (var b in bList)
            {
                if (na == b.BusinessName)
                {
                    return b;
                }
            }
            return null;
        }
        
        // Search Facilities
        static SHNFacility SearchFacility(List<SHNFacility> sList, string na)
        {
            foreach (var s in sList)
            {
                if (na == s.FacilityName)
                {
                    return s;
                }
            }
            return null;
        }
        
        // Other functions 

        // Add travel entry from csv
        static void AddTravelEntryList(List<SHNFacility>sList,Person addto,string last, string entrym, string ed, string edd, string paid, string facilname)
        {
            string lastcon = last;
            string entmode = entrym;
            DateTime entryd = DateTime.ParseExact(ed,"dd/MM/yyyy HH:mm", null);
            DateTime entdate = entryd;
            TravelEntry addte = new TravelEntry(lastcon, entmode, entdate);
            if (facilname != "")
            {       
                addte.ShnStay = SearchFacility(sList, facilname);
            }
            if (paid != "")
            {
                bool tf = Convert.ToBoolean(paid);
                addte.isPaid = tf;
            }

            if (edd != "")
            {
                DateTime endd = DateTime.ParseExact(edd,"dd/MM/yyyy HH:mm", null);
                addte.ShnEndDate = endd;
            }
            addto.AddTravelEntry(addte);
        }

        // Menu option functions:
        
        // 1. Load Person data
        static void LoadPerson(List<Person>pList, List<SHNFacility>sList)
        {
            string[] personDetails = File.ReadAllLines("Person.csv");
            for (int i = 1; i < personDetails.Length; i++)
            {
                string[] pDetails = personDetails[i].Split(',');
                string name = pDetails[1];
                // Resident
                if (pDetails[0] == "resident")
                {
                    string address = pDetails[2];
                    DateTime lastleft = DateTime.ParseExact((pDetails[3]),"dd/MM/yyyy", null);
                    Resident newr = new Resident(name, address, lastleft);
                    pList.Add(newr);
                    //Token
                    if (pDetails[6] != "")
                    {
                        string serial = pDetails[6];
                        string collection = pDetails[7];
                        DateTime exdate = Convert.ToDateTime(pDetails[8]);
                        TraceTogetherToken newt = new TraceTogetherToken(serial, collection, exdate);
                        newr.Token = newt;
                    }
                    if (pDetails[9] != "")
                    {
                        AddTravelEntryList(sList, newr,pDetails[9],pDetails[10], pDetails[11],pDetails[12],pDetails[13], pDetails[14]);
                    }
                }
                // Visitor
                else if (pDetails[0] == "visitor")
                {
                    string passport = pDetails[4];
                    string nationality = pDetails[5];
                    Visitor newv = new Visitor(name, passport, nationality);
                    pList.Add(newv);
                    if (pDetails[9] != "")
                    {
                        AddTravelEntryList(sList, newv,pDetails[9],pDetails[10], pDetails[11],pDetails[12],pDetails[13], pDetails[14]);
                    }
                }
            }
            Console.WriteLine("Loaded Person Data");
        }

        // 1. Load Business Location data
        static void LoadBusiness(List<BusinessLocation> bList)
        {
            string[] bizDetails = File.ReadAllLines("BusinessLocation.csv");
            for (int i = 1; i < bizDetails.Length; i++)
            {
                string[] bDetails = bizDetails[i].Split(',');
                string name = bDetails[0];
                string code = bDetails[1];
                int cap = Convert.ToInt32(bDetails[2]);
                BusinessLocation business = new BusinessLocation(name, code, cap);

                bList.Add(business);
            }
            Console.WriteLine("Loaded Business Location Data");
        }

        // 2. Load SHN data
        static List<SHNFacility> LoadShnData(List<SHNFacility> facilityList)
        {
            using (HttpClient client = new HttpClient())
            {
                List<SHNFacility> sList = new List<SHNFacility>();
                client.BaseAddress = new Uri("https://covidmonitoringapiprg2.azurewebsites.net");
                Task<HttpResponseMessage> responseTask = client.GetAsync("/facility");
                responseTask.Wait();
                HttpResponseMessage result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    Task<string> readTask = result.Content.ReadAsStringAsync();
                    readTask.Wait();
                    string data = readTask.Result;
                    sList = JsonConvert.DeserializeObject<List<SHNFacility>>(data);
                }
                foreach (SHNFacility s in sList)
                {
                    facilityList.Add(s);
                }
            }
            Console.WriteLine("Loaded SHN Facility Data");
            return facilityList;
        }

        // 3. List Visitors
        static void ListVisitors(List<Person> pList)
        {
            Console.WriteLine("{0,-10}{1,-20}{2,10}", "Name" , "Passport No", "Nationality");
            foreach (Person p in pList)
            {
                if (p is Visitor)
                {
                    Console.WriteLine(p.ToString());
                }
            }

        }

        // 4. List Person Details
        static void ListDetails(List<Person> pList, string search)
        {
            if (SearchPerson(pList, search) is null)
            {
                Console.WriteLine("Sorry, this person does not exist");
            }
            else
            {
                Person p = SearchPerson(pList, search);
                if (p is Visitor)
                {
                    Console.WriteLine("{0,-10}{1,-20}{2,10}", "Name", "Passport No", "Nationality");
                    Console.WriteLine(p.ToString());
                    
                    Console.WriteLine();
                    if (p.TravelEntryList is null)
                    {
                        Console.WriteLine("User does not have Travel Entry Data");

                    }
                    else
                    {
                        Console.WriteLine("{0,-30}{1,-15}{2,-15}", "Last Country of Embarkation", "Entry Mode",
                            "Entry Date");
                        foreach (var t in p.TravelEntryList)
                        {
                            Console.WriteLine("{0,-30}{1,-15}{2,-15}", t.LastCountryOfEmbarkation, t.EntryMode,
                                t.EntryDate);
                        }
                    }
                    
                    Console.WriteLine();
                    if (p.SafeEntryList is null)
                    {
                        Console.WriteLine("User does not have SafeEntry Data");
                    }
                    else
                    {
                        foreach (var s in p.SafeEntryList)
                        {
                            if (s.CheckOut == new DateTime())
                            {
                                Console.WriteLine("SafeEntry that have not Checked out");
                                Console.WriteLine("{0,-25}{2,-20}", "Check In", "Location");
                                Console.WriteLine("{0,-25}{2,-20}", s.CheckIn, s.Location.BusinessName);
                            }
                            else
                            {
                                Console.WriteLine("SafeEntry that have Checked out");
                                Console.WriteLine("{0,-25}{1,-20}{2,-20}", "Check In", "Check Out", "Location");
                                Console.WriteLine("{0,-25}{1,-20}{2,-20}", s.CheckIn, s.CheckOut, s.Location.BusinessName);
                            }

                        }
                    }
                }
                else
                {
                    Console.WriteLine("{0,-10}{1,-25}{2,10}", "Name", "Address", "Last Left Country");
                    Console.WriteLine(p.ToString());

                    Console.WriteLine();
                    if (p.TravelEntryList is null)
                    {
                        Console.WriteLine();
                        Console.WriteLine("User does not have Travel Entry Data");
                    }
                    else
                    {
                        foreach (var t in p.TravelEntryList)
                        {
                            Console.WriteLine("{0,-30}{1,-15}{2,-15}", "Last Country of Embarkation", "Entry Mode",
                                "Entry Date");
                            Console.WriteLine("{0,-30}{1,-15}{2,-15}", t.LastCountryOfEmbarkation, t.EntryMode,
                                t.EntryDate);
                        }
                    }
                    
                    Console.WriteLine();
                    if (p.SafeEntryList is null)
                    {
                        Console.WriteLine("User does not have SafeEntry Data");
                    }
                    else
                    {
                        foreach (var s in p.SafeEntryList)
                        {
                            if (s.CheckOut == new DateTime())
                            {
                                Console.WriteLine("SafeEntry that have not Checked out");
                                Console.WriteLine("{0,-25}{2,-20}", "Check In", "Location");
                                Console.WriteLine("{0,-25}{2,-20}", s.CheckIn, s.Location.BusinessName);
                            }
                            else
                            {
                                Console.WriteLine("SafeEntry that have Checked out");
                                Console.WriteLine("{0,-25}{1,-25}{2,-20}", "Check In", "Check Out", "Location");
                                Console.WriteLine("{0,-25}{1,-25}{2,-20}", s.CheckIn, s.CheckOut, s.Location.BusinessName);
                            }
                            
                        }
                    }
                    
                    Console.WriteLine();
                    Resident r = (Resident) p;
                    if (r.Token is null)
                    {
                        Console.WriteLine("User does not have a Trace Together Token");
                    }
                    else
                    {
                        Console.WriteLine("{0,-15}{1,-25}{2,-20}", "SerialNo", "Collection Location", "Expiry Date");
                        Console.WriteLine("{0,-15}{1,-25}{2,-20}", r.Token.SerialNo, r.Token.CollectionLocation,
                            r.Token.ExpiryDate);
                    }
                }
            }
        }

        // 5. Assign/ReplaceToken
        static void AssignOrReplaceToken(List<Person> pList)
        {
            try
            {
                Console.Write("Enter your name: ");
                string search = Console.ReadLine();
                if (SearchPerson(pList, search) is null)
                {
                    Console.WriteLine("Sorry, this person does not exist");
                }
                else
                {
                    Person p = SearchPerson(pList, search);
                    if (p is Resident)
                    {
                        Resident r = (Resident)p;
                        if (r.Token == null)
                        {
                            AssignToken(pList, r);
                        }
                        else if (r.Token != null)
                        {
                            if (r.Token.IsEligibleForReplacement())
                            {
                                r.Token.ReplaceToken();
                            }
                            else
                            {
                                Console.WriteLine("Sorry, your token is not eligible for replacement");
                            }

                        }
                        else
                        {
                            Console.WriteLine("Invalid Input");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Sorry, this person is not a Resident");
                    }
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Sorry, that was the wrong input");
            }
            
            
        }


        // 6. List Businesses
        static void ListBusiness(List<BusinessLocation> bList)
        {
            Console.WriteLine("{0,-22}{1,-20}{2,-20}{3,-20}", "Business Name" , "Branch Code", "Maximum Capacity", "Visitors Now");
            foreach (BusinessLocation b in bList)
            {
                Console.WriteLine(b.ToString());
            }
        }
        

        // 7. Edit Business Location Capacity
        static void EditCapacity(List<BusinessLocation> bList)
        {
            try
            {
                Console.Write("Please enter Business Name: ");
                string search = Console.ReadLine();
                if (SearchBusiness(bList, search) == null)
                {
                    Console.WriteLine("This business name does not exist");
                }
                else
                {
                    BusinessLocation b = SearchBusiness(bList, search);
                    Console.Write("Enter new Maximum Capacity: ");
                    int new_max = Convert.ToInt32(Console.ReadLine());
                    b.MaximumCapacity = new_max;
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Sorry, that is an invalid input");
            }
            

        }

        // 8. SafeEntry check in
        static void CheckIn(List<Person> pList, List<BusinessLocation> bList)
        {
            try
            {
                Console.Write("Enter your name: ");
                string search = Console.ReadLine();
                Person p = SearchPerson(pList, search);
                ListBusiness(bList);
                Console.WriteLine("Which Business Location would you like to check into?");
                string b_loc = Console.ReadLine();
                BusinessLocation b = SearchBusiness(bList, b_loc);
                if (b.VisitorsNow < b.MaximumCapacity)
                {
                    b.VisitorsNow++;
                    SafeEntry new_s = new SafeEntry(DateTime.Now, b);
                    p.AddSafeEntry(new_s);
                }
                else
                {
                    Console.WriteLine("This business is already full");
                }
            }
            catch(NullReferenceException)
            {
                Console.WriteLine("Sorry, you entered the wrong business name");
            }
            catch (FormatException)
            {
                Console.WriteLine("Invalid Input");
            }
            
        }

        // 9. SafeEntry check out
        static void CheckOut(List<Person> pList)
        {
            try
            {
                Console.Write("Enter your name: ");
                string search = Console.ReadLine();
                Person p = SearchPerson(pList, search);
                if (p.SafeEntryList is null)
                {
                    Console.WriteLine("Sorry, this person does not have any SafeEntry data");
                }
                else
                {
                    foreach (SafeEntry s in p.SafeEntryList)
                    {
                        if (s.CheckOut == new DateTime())
                        {
                            Console.WriteLine("{0,-25}{1,-20}", "Check In", "Location");
                            Console.WriteLine("{0,-25}{1,-20}", s.CheckIn, s.Location.BusinessName);
                        }
                    }
                    Console.Write("Enter location to Check Out of:");
                    string s_loc = Console.ReadLine();

                    foreach (SafeEntry s in p.SafeEntryList)
                    {
                        if (s.Location.BusinessName == s_loc)
                        {
                            s.PerformCheckOut();
                            s.Location.VisitorsNow--;
                            return;
                        }
                    }
                }
                
            }
            catch (NullReferenceException)
            {
                Console.WriteLine("Sorry, you entered the wrong business name");
            }
            catch (FormatException)
            {
                Console.WriteLine("Invalid Input");
            }
        }
        
        // Find Serial Number
        static string FindSerial(List<Person> plist)
        {
            
            Random rand = new Random();
            int test_no = rand.Next(1000, 9999);
            string new_serial = 'T' + Convert.ToString(test_no);
            
            while (true)
            {
                if (CheckSerial(plist, new_serial) == false)
                {
                    test_no = rand.Next(1000, 9999);
                    new_serial = 'T' + Convert.ToString(test_no);
                }
                else
                {
                    return new_serial;
                }
            }
        }
        
        // Check Serial Number
        static bool CheckSerial(List<Person> plist, string sn)
        {
            foreach (Person p in plist)
            {
                if (p is Resident)
                {
                    Resident r = (Resident)p;
                    if (r.Token != null)
                    {
                        if (sn == r.Token.SerialNo)
                        {
                            return false;
                        }
                    }
                    
                }
                
            }
            return true;
        }
        
        // Assign Token
        static void AssignToken(List<Person> plist, Resident r)
        {
            try
            {
                string serialnumber = Convert.ToString(FindSerial(plist));
                Console.Write("Enter Collection Location for this token: ");
                string col_loc = Console.ReadLine();
                DateTime new_expiry = DateTime.Now.AddMonths(6);

                TraceTogetherToken t1 = new TraceTogetherToken(serialnumber, col_loc, new_expiry);

                r.Token = t1;
            }
            catch(FormatException)
            {
                Console.WriteLine("Invalid input");
            }
            

        }
        
        // 10. List SHN facilities

        static void ListShn(List<SHNFacility>sList)
        {
            foreach (SHNFacility s in sList)
            {
                Console.WriteLine("{0,-20}  {1,-14}  {2,-13}  {3,-14}  {4,0}",
                    s.FacilityName, s.FacilityCapacity, s.distFromAirCheckpoint, s.distFromLandCheckpoint, s.distFromSeaCheckpoint);
            }
        }

        
        // 11. Create Visitor
        static void CreateVisitor(List<Person>pList)
        {
            Console.Write("Enter your name: ");
            string vname = Console.ReadLine();
            Console.Write("Enter your passport number: ");
            string vpassport = Console.ReadLine();
            Console.Write("Enter your nationality: ");
            string vnationality = Console.ReadLine();
            Visitor newv = new Visitor(vname, vpassport, vnationality);
            pList.Add(newv);
            Console.WriteLine("Successfully created");
        }
        
        // 12. Create TravelEntry Record
        static void CreateTravelEntry(List<Person>pList, List<SHNFacility>sList)
        {
            Console.Write("Enter your name: ");
            string visitorname = Console.ReadLine();
            Person newvrecord = SearchPerson(pList, visitorname);
            if (newvrecord is null)
            {
                Console.WriteLine("Invalid Input");
            }
            else
            {
                Console.Write("Enter last country of embarkation: ");
                string lc = Console.ReadLine();
                Console.Write("Enter entry mode: ");
                string em = Console.ReadLine();
                DateTime etd = DateTime.Now;
                TravelEntry newrecord = new TravelEntry(lc, em,etd);
                newrecord.CalculateSHNDuration();
                int duration = (newrecord.ShnEndDate - etd).Days;
                if (duration >= 7)
                {
                    ListShn(sList);
                    Console.Write("Select SHN Facility: ");
                    string chosen = Console.ReadLine();
                    SHNFacility s = SearchFacility(sList, chosen);
                    if (s is null)
                    {
                        Console.WriteLine("Invalid Input");
                    }
                    else
                    {
                        newrecord.AssignSHNFacility(s);
                        newvrecord.AddTravelEntry(newrecord);
                        Console.WriteLine("Successfully added");
                    }
                }
                else
                {
                    newvrecord.AddTravelEntry(newrecord);
                    Console.WriteLine("Successfully added");
                }
            }
        }
        
        // 13. Calculate SHN Charges
        static void CalculateShnCharges(List<Person>pList)
        {
            try
            {         
                Console.Write("Enter your name: ");
                string n = Console.ReadLine();
                Person vcharges = SearchPerson(pList, n);
                
                foreach (TravelEntry t in vcharges.TravelEntryList)
                {
                    if (t.ShnEndDate < DateTime.Today)
                    {
                        if (t.isPaid == false)
                        {
                            double shncharge;
                            shncharge = vcharges.CalculateSHNCharges(t);
                            if (t.ShnStay != null)
                            {
                                shncharge = shncharge + t.ShnStay.CalculateTravelCost(t.EntryMode, t.EntryDate);
                            }
                           
                            shncharge = shncharge * 1.07;
                            Console.WriteLine("${0:0.00} due", shncharge);

                            Console.WriteLine("Please make payment!");
                            t.isPaid = true;
                        }
                        else
                        {
                            Console.WriteLine("No outstanding charges");
                        }
                    }
                    else
                    {
                        Console.WriteLine("No outstanding charges");
                    }
                }
            }
            catch (NullReferenceException)
            {
                Console.WriteLine("This person does not exist.");
            }
        }

        // 14. Contact Tracing Report
        static void ContactTracingReport(List<Person> pList)
        {
            try
            {
                Console.Write("Enter and Date and Time: ");
                DateTime option_dt = Convert.ToDateTime(Console.ReadLine());
                Console.Write("Enter a Business Name: ");
                string option_na = Console.ReadLine();
                using (StreamWriter sw = new StreamWriter("ContactTracing.csv", true))
                {
                    sw.WriteLine("Contact Tracing at" + "," + option_na);
                    sw.WriteLine("Name" + "," + "Check In" + "," + "Check Out");
                }
                Console.WriteLine("{0,-15}{1,-25}{2,-25}", "Name", "Check In", "Check Out");
                foreach (Person p in pList)
                {
                    foreach (SafeEntry s in p.SafeEntryList)
                    {

                        if (CheckSameDate(option_dt, s.CheckIn) || s.Location.BusinessName == option_na)
                        {
                            using (StreamWriter sw = new StreamWriter("ContactTracing.csv", true))
                            {
                                if (s.CheckOut == new DateTime())
                                {
                                    sw.WriteLine(p.Name + "," + s.CheckIn + "," + "Not Checked Out");
                                    Console.WriteLine("{0,-15}{1,-25}{2,-25}", p.Name, s.CheckIn, "Not Checked Out");
                                }
                                else
                                {
                                    sw.WriteLine(p.Name + "," + s.CheckIn + "," + s.CheckOut);
                                    Console.WriteLine("{0,-15}{1,-25}{2,-25}", p.Name, s.CheckIn, s.CheckOut);
                                }
                            }
                        }
                        else if (CheckSameDate(option_dt, s.CheckOut) || s.Location.BusinessName == option_na)
                        {
                            using (StreamWriter sw = new StreamWriter("ContactTracing.csv", true))
                            {
                                if (s.CheckOut == new DateTime())
                                {
                                    sw.WriteLine(p.Name + "," + s.CheckIn + "," + "Not Checked Out");
                                    Console.WriteLine("{0,-15}{1,-25}{2,-25}", p.Name, s.CheckIn, "Not Checked Out");
                                }
                                else
                                {
                                    sw.WriteLine(p.Name + "," + s.CheckIn + "," + s.CheckOut);
                                    Console.WriteLine("{0,-15}{1,-25}{2,-25}", p.Name, s.CheckIn, s.CheckOut);
                                }
                            }
                        }
                    }
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Sorry, that input was invalidt");
            }
            catch (NullReferenceException)
            {
                Console.WriteLine("That business name does not exists");
            }
            
        }

        static bool CheckSameDate(DateTime check_date, DateTime actual_date)
        {
            if (check_date.Day == actual_date.Day && check_date.Month == actual_date.Month && check_date.Year == actual_date.Year)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        // 15. SHN Status Reporting
        static void ShnStatusReport(List<Person>pList)
        {
            using (StreamWriter sw = new StreamWriter("StatusReport.csv", true))
            {
                sw.WriteLine("Name" + "," + "SHN End Date" + "," + "SHN Facility");
            }

            try
            {
                Console.Write("Enter date: ");
                DateTime date = DateTime.ParseExact(Console.ReadLine(),"dd/MM/yyyy", null);
                for (int i = 0; i < pList.Count; i++)
                {
                    for (int t = 0; t < pList[i].TravelEntryList.Count; t++)
                    {
                        if (pList[i].TravelEntryList[t].ShnEndDate.Day > date.Day)
                        {
                            if ((pList[i].TravelEntryList[t].ShnEndDate - pList[i].TravelEntryList[t].EntryDate).TotalDays == 14)
                            {
                                string personName = pList[i].Name;
                                DateTime personDate = pList[i].TravelEntryList[t].ShnEndDate;
                                string facil = pList[i].TravelEntryList[t].ShnStay.FacilityName;
                                using (StreamWriter sw = new StreamWriter("StatusReport.csv", true))
                                {
                                    sw.WriteLine(personName + "," + personDate + "," + facil);
                                }
                            }
                        }
                    }
                } 
            }
            catch (FormatException)
            {
                Console.WriteLine("Invalid DateTime input");
            }
        }

        // 16. Create new Business Location
        static void CreateBusiness(List<BusinessLocation> bList)
        {
            try
            {
                Console.Write("Enter business name: ");
                string bname = Console.ReadLine();
                Console.Write("Enter branch code: ");
                string bcode = Console.ReadLine();
                Console.Write("Enter capacity: ");
                int bcapacity = Convert.ToInt32(Console.ReadLine());
                BusinessLocation loc = new BusinessLocation(bname, bcode, bcapacity);
                bList.Add(loc);
            }
            catch(FormatException)
            {
                Console.WriteLine("Invalid Input");
            }
            
        }

        // 17. Create New Facility
        static void CreateFacility(List<SHNFacility> sList)
        {
            try
            {
                Console.Write("Enter facility name:  ");
                string facilname = Console.ReadLine();
                Console.Write("Enter facility capacity: ");
                int facilcap = Convert.ToInt32(Console.ReadLine());
                Console.Write("Enter distance from air checkpoint: ");
                double distair = Convert.ToDouble(Console.ReadLine());
                Console.Write("Enter distance from land checkpoint: ");
                double distland = Convert.ToDouble(Console.ReadLine());
                Console.Write("Enter distance from sea checkpoint: ");
                double distsea = Convert.ToDouble(Console.ReadLine());
                SHNFacility facil = new SHNFacility(facilname, facilcap, distair, distland, distsea);
                sList.Add(facil);
            }
            catch (FormatException)
            {
                Console.WriteLine("Invalid Input");
            }
        }

        // 18. List Persons in each Facility
        static void ListFacility(List<SHNFacility> sList, List<Person>pList)
        {
            try
            {
                Console.Write("Enter facility name:");
                string facilname = Console.ReadLine();
                SHNFacility facil = SearchFacility(sList, facilname);
                Console.WriteLine("Persons staying in {0}", facilname);
                Console.WriteLine("{0,-10} {1,5}", "Name", "SHN End Date");
                foreach (var p in pList)
                {
                    foreach (var t in p.TravelEntryList)
                    {
                        if (t.ShnStay != null)
                        {
                            if (t.ShnStay.FacilityName == facilname)
                            {
                                Console.WriteLine("{0,-10} {1,5}", p.Name, t.ShnEndDate);
                            }
                        }
                            
                    }
                        
                }

            }
            catch(NullReferenceException)
            {
                Console.WriteLine("This facility does not exist");
            }
            catch (FormatException)
            {
                Console.WriteLine("Invalid Input");
            }

        }


        // 19. Move people from one facility to the other
        static void MoveFacility(List<Person> pList, List<SHNFacility> sList)
        {
            ListFacility(sList,pList);
            try
            {
                Console.Write("Enter name of person to be shifted: ");
                string m_name = Console.ReadLine();
                Person p = SearchPerson(pList, m_name);

                Console.Write("Enter name of new facility: ");
                string m_facil = Console.ReadLine();

                SHNFacility new_facil = SearchFacility(sList, m_facil);

                foreach (TravelEntry t in p.TravelEntryList)
                {
                    SHNFacility current_facil = t.ShnStay;
                }


                Console.WriteLine("Current Facil Price is {0:0.00}: ", NewShnCharges(pList, m_name));


                TravelEntry tr = p.TravelEntryList[p.TravelEntryList.Count - 1];
                tr.AssignSHNFacility(new_facil);
                Console.WriteLine("New Facil Price is {0:0.00}: ", (NewShnCharges(pList, m_name) + 500));

                Console.WriteLine("Do you want to change Facility?");
                Console.Write("Yes or No: ");
                string option = Console.ReadLine();

                if (option == "Yes")
                {
                    TravelEntry t = p.TravelEntryList[p.TravelEntryList.Count - 1];
                    t.AssignSHNFacility(new_facil);
                }

                else if (option == "No")
                {

                }
            }
            catch(FormatException)
            {
                Console.WriteLine("Invalid Input");
            }
            catch(NullReferenceException)
            {
                Console.WriteLine("Person or Facility does not exist");
            }
            

        }
        static double NewShnCharges(List<Person> pList, string name)
        {
            Person chargeperson = SearchPerson(pList, name);

            double shncharge = 0;

            foreach (TravelEntry t in chargeperson.TravelEntryList)
            {
                shncharge = chargeperson.CalculateSHNCharges(t);
                if (t.ShnStay != null)
                {
                    shncharge = shncharge + t.ShnStay.CalculateTravelCost(t.EntryMode, t.EntryDate);
                }

                shncharge = shncharge * 1.07;

                t.isPaid = true;
                return shncharge;
            }
            return shncharge;
        }
    }
}


