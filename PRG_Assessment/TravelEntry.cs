﻿
//============================================================
// Student Number : S10204599
// Student Name : Ng Lynn Shaan
// Module Group : T06
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace PRG_Assessment
{
    class TravelEntry
    {
        public string LastCountryOfEmbarkation { get; set; }
        public string EntryMode { get; set; }
        public DateTime EntryDate { get; set; }
        public DateTime ShnEndDate { get; set; }
        public SHNFacility ShnStay { get; set; }
        public bool isPaid { get; set; }
        
        public TravelEntry()
        {

        }
        public TravelEntry(string lcoe, string entm, DateTime entd)
        {
            LastCountryOfEmbarkation = lcoe;
            EntryMode = entm;
            EntryDate = entd;
            isPaid = false;
        }
        public void AssignSHNFacility(SHNFacility sf)
        {
            ShnStay = sf;
            sf.FacilityCapacity = sf.FacilityCapacity - 1;
        }
        public void CalculateSHNDuration()
        {
            if (LastCountryOfEmbarkation == "New Zealand" || LastCountryOfEmbarkation == "Vietnam")
            {
                ShnEndDate = EntryDate;
            }
            else if (LastCountryOfEmbarkation == "Macao SAR")
            {
                ShnEndDate = EntryDate.AddDays(7);
            }
            else
            {
                ShnEndDate = EntryDate.AddDays(14);
            }
        }
        public override string ToString()
        {
            return base.ToString();
        }
    }
}
