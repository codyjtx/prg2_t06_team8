﻿
//============================================================
// Student Number : S10155754, S10204599
// Student Name : Jeow Teng Xiang Cody, Ng Lynn Shaan
// Module Group : T06
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace PRG_Assessment
{
    class TraceTogetherToken
    {
        public string SerialNo { get; set; }
        public string CollectionLocation { get; set; }
        public DateTime ExpiryDate { get; set; }

        public TraceTogetherToken()
        {

        }
        public TraceTogetherToken(string sn, string cl, DateTime ed)
        {
            SerialNo = sn;
            CollectionLocation = cl;
            ExpiryDate = ed;
        }
        public bool IsEligibleForReplacement()
        {
            DateTime today = DateTime.Now;
            if (today < ExpiryDate)
            {
                if (today > ExpiryDate.AddMonths(-1))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        public void ReplaceToken()
        {
            if (IsEligibleForReplacement())
            {
                ExpiryDate = DateTime.Now.AddMonths(6);
                Console.WriteLine("Your Token now has another 6 months before expiring.");
            }
            else
            {
                Console.WriteLine("Sorry, your token is not Eligible for replacement.");
            }
        }

        public override string ToString()
        {
            return base.ToString() + String.Format("{0,-10}{1,-20}{2,10}", SerialNo, CollectionLocation, ExpiryDate);
        }
    }
}
