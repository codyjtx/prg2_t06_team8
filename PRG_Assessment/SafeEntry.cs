﻿
//============================================================
// Student Number : S10155754
// Student Name : Jeow Teng Xiang Cody
// Module Group : T06
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace PRG_Assessment
{
    class SafeEntry
    {
        public DateTime CheckIn { get; set; }
        public DateTime CheckOut { get; set; }
        public BusinessLocation Location { get; set; }
        public SafeEntry()
        {

        }
        public SafeEntry(DateTime ci, BusinessLocation lo)
        {
            CheckIn = ci;
            Location = lo;
        }
        public void PerformCheckOut()
        {
            CheckOut = DateTime.Now;
        }
        public override string ToString()
        {
            return base.ToString() + String.Format("{0,-15}{1,-15}", CheckIn, Location);
        }
    }
}
