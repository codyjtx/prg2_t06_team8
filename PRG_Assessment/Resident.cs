﻿
//============================================================
// Student Number : S10155754, S10204599
// Student Name : Jeow Teng Xiang Cody, Ng Lynn Shaan
// Module Group : T06
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace PRG_Assessment
{
    class Resident : Person
    {
        public string Address { get; set; }
        public DateTime LastLeftCountry { get; set; }
        public TraceTogetherToken Token { get; set; }
        public Resident(string n, string a, DateTime l): base( n)
        {
            Address = a;
            LastLeftCountry = l;
        }
        public override double CalculateSHNCharges(TravelEntry t)
        {
            int difference = (t.ShnEndDate - t.EntryDate).Days;
            if (difference == 0)
            {
                return 200;
            }
            else if (difference == 7)
            {
                return 200 + 20;
            }
            else 
            {
                return 200 + 20 + 1000;
            }
        }
        public override string ToString()
        {
            return base.ToString() + string.Format("{0,-25}{1,10}", Address, LastLeftCountry);
        }
    }
}
