﻿
//============================================================
// Student Number : S10155754, S10204599
// Student Name : Jeow Teng Xiang Cody, Ng Lynn Shaan
// Module Group : T06
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace PRG_Assessment
{
    abstract class Person
    {
        public string Name { get; set; }
        public List<SafeEntry>SafeEntryList { get; set; }
        public List<TravelEntry>TravelEntryList { get; set; }

        public Person()
        {

        }

        public Person(string n)
        {
            Name = n;
            SafeEntryList = new List<SafeEntry>();
            TravelEntryList = new List<TravelEntry>();

        }
        public void AddTravelEntry(TravelEntry t)
        {
            TravelEntryList.Add(t);
        }
        public void AddSafeEntry(SafeEntry s)
        {
            SafeEntryList.Add(s);
        }

        public abstract double CalculateSHNCharges(TravelEntry t);

        public override string ToString()
        {
            return string.Format("{0,-10}", Name);
        }


    }
}
